/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deckofcards;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jeffersonbienaime
 */
public class DeckTest {

    /**
     * Test to Verify if the Deck initialize correctly.
     */
    @Test
    public void deck_initializesCorrectly() {
        Deck testDeck = new Deck();
        assertEquals(true, testDeck instanceof Deck);
    }

    /**
     * Test to Verify if the Deck load all cards correctly.
     */
    @Test
    public void getCards_DeckShouldListAllCards() {
        Deck testDeck = new Deck();
        assertEquals(true, testDeck.getCards() instanceof List);
    }

    /**
     * Test to Verify if the Deck has indeed 52 cards .
     */
    @Test
    public void getCards_DeckShouldHave52Cards() {
        Deck testDeck = new Deck();
        assertEquals(52, testDeck.getCards().size());
    }

    /**
     * Test to Verify if the Deck starts with Ace of Spades correctly.
     */
    @Test
    public void getCards_DeckStartsWithAceOfSpades() {
        Deck testDeck = new Deck();
        Card aceOfSpades = testDeck.getCards().get(0);
        assertEquals("ACE of SPADES", aceOfSpades.toString());
    }

    /**
     * Test to Verify if the Deck contains a unique Two of Hearts.
     */
    @Test
    public void getCards_deckContainsUniqueCards_TwoOfHearts() {
        Deck testDeck = new Deck();
        Card twoOfHearts = testDeck.getCards().get(51);
        assertEquals("TWO of HEARTS", twoOfHearts.toString());
    }

    /**
     * Test of shuffle to verify if Deck doesn't have a duplicate card .
     */
    @Test
    public void testShuffleIfIhaveDuplicateCards() {
        System.out.println("shuffle");
        Deck instance = new Deck();
        instance.shuffle();
        List listcards = Arrays.asList(instance.getCards());
        if (listcards.stream().distinct().count() == 0) {
            assertTrue(true);
        } else {
            fail("Duplicate Card");
        }
    }

}
