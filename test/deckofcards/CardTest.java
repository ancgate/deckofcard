/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deckofcards;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jeffersonbienaime
 */
public class CardTest {
    
    public CardTest() {
    }
    
@Test
  public void verify_CardInitializeCorrectly() {
    Card testCard = new Card(Suit.SPADES, Rank.ACE);
    assertEquals(true, testCard instanceof Card);
  }

  @Test
  public void getSuit_returnsACardsSuit_Spades() {
    Card testCard = new Card(Suit.SPADES, Rank.ACE);
    assertEquals(Suit.SPADES, testCard.getSuit());
  }

  @Test
  public void getRank_returnsACardsRank_Queen() {
    Card testCard = new Card(Suit.DIAMONDS, Rank.QUEEN);
    assertEquals(Rank.QUEEN, testCard.getRank());
  }

  @Test
  public void returnNameOfTheCardFormattedACEOfSPADES() {
    Card testCard = new Card(Suit.SPADES, Rank.ACE);
    assertEquals("ACE of SPADES", testCard.toString());
    System.out.println(testCard.toString());
    
}
    
}
