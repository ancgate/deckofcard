/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deckofcards;

/**
 *
 * @author jeffersonbienaime
 */
public class Card {
    
    private final Suit suit;
    private final Rank rank;

    /**
     *
     * @param suit
     * @param rank
     */
    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    /**
     *
     * @return a suit
     */
    public Suit getSuit() {
        return suit;
    }

    /**
     *
     * @return a rank
     */
    public Rank getRank() {
        return rank;
    }
    
    @Override
        public String toString() {
            return rank+" of "+suit;
       }
            
    
}
