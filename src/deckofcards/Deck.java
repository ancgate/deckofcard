/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deckofcards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author jeffersonbienaime
 */
public class Deck {

    /**
     *
     */
    public int sizeofDeck;
    private final Suit suits[] = Suit.values();
    private final Rank ranks[] = Rank.values();
    private List<Card> cards;

    /**
     *
     */
    public Deck() {

    }

    /**
     * Shuffle method
     */
    public void shuffle() {
        Collections.shuffle(cards);
    }

    /**
     *
     * @return a list of cards
     */
    public List<Card> getCards() {
        return cards;
    }

    public Long duplicateCards() {
        return cards.stream().distinct().count();
    }

    public void populateCards() {

        cards = new ArrayList<>();
        for (Suit suit : suits) {
            for (Rank rank : ranks) {
                cards.add(new Card(suit, rank));
            }
        }

    }
    
    
    public void resetDeck(){
     
        cards.clear();
    }
        

}
