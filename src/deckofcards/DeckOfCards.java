/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deckofcards;

/**
 *
 * @author jeffersonbienaime
 */
public class DeckOfCards {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Deck deck = new Deck();
        System.out.println("-------------------------------");
        System.out.println("------- DECK OF CARDS  --------");
        System.out.println("-------------------------------");
        System.out.println();
        System.out.println("Deck Before shuffle: ");
        System.out.println("Size of Deck");
        deck.populateCards();
        System.out.println(deck.getCards().size());
        System.out.println();
        try {
            System.out.println("Shuffling");
            deck.shuffle();
            deck.getCards().stream().forEach((c) -> {
                System.out.println(c.toString());
            });
            if (deck.duplicateCards() == 1) {
                System.out.println("--------DUPLICATE CARDS ON THE DECK--------");
            } else {
                System.out.println("--No Duplicate Cards on the Deck--");
            }
        } catch (Exception e) {
            System.err.println("Error shuffling deck!");
            System.out.println();
            System.err.println(e.getMessage());
            System.exit(1);
        }
        System.out.println("Deck After shuffle: ");
        deck.resetDeck();
        
        System.out.println("-------------------------------");
        System.out.println("------- DECK OF CARDS 2 --------");
        System.out.println("-------------------------------");
        System.out.println();
        System.out.println("Deck Before shuffle: ");
        System.out.println();
        try {
        System.out.println("Size of Deck");
        System.out.println(deck.getCards().size());
        deck.shuffle();
        deck.getCards().stream().forEach((c) -> {
                System.out.println(c.toString());
            });
        } catch (Exception e) {
            System.err.println("Empty Deck!");
            System.out.println();
            System.err.println(e.getMessage());
            System.exit(1);
        }
        System.out.println("Deck After shuffle: ");
        
        
        
        

    }

}
